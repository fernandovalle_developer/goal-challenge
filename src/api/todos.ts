import axios from 'axios';
import { Todo } from '@/store/todo/types';

export default {
  BASE_URL: 'http://localhost:3000/todos',

  async getTodos() : Promise<Todo[]> {
    return new Promise<Todo[]>((resolve, reject) => {
      const url = this.BASE_URL;
      axios.get(url).then((response) => {
        resolve(response.data as Todo[]);
      }, (err) => {
        reject(err);
      });
    });
  },

  async get(todo:Todo) : Promise<Todo> {
    return new Promise<Todo>((resolve, reject) => {
      const url = `${this.BASE_URL}/${todo.id}`;
      axios.get(url).then((response) => {
        resolve(response.data as Todo);
      }, (err) => {
        reject(err);
      });
    });
  },

  async insert(todo:Todo) : Promise<Todo> {
    return new Promise<Todo>((resolve, reject) => {
      const url = `${this.BASE_URL}`;
      axios.post(url, todo).then((response) => {
        resolve(response.data as Todo);
      }, (err) => {
        reject(err);
      });
    });
  },

  async update(todo:Todo) : Promise<Todo> {
    return new Promise<Todo>((resolve, reject) => {
      const url = `${this.BASE_URL}/${todo.id}`;
      axios.put(url, todo).then((response) => {
        resolve(response.data as Todo);
      }, (err) => {
        reject(err);
      });
    });
  },

  async delete(todo:Todo) : Promise<object> {
    return new Promise<object>((resolve, reject) => {
      const url = `${this.BASE_URL}/${todo.id}`;
      axios.delete(url).then((response) => {
        resolve(response.data as object);
      }, (err) => {
        reject(err);
      });
    });
  },

  async clearCompleted(todos:Todo[]) : Promise<object> {
    const toBeDeleted = todos.filter((todo:Todo) => todo.completed).map((todo:Todo) => this.delete(todo));
    return Promise.all(toBeDeleted);
  },

  async completedUncompletedTodos(todos:Todo[], completed:boolean) : Promise<object> {
    const toBeUpdated = todos.map((todo:Todo) => this.update(new Todo(todo.text, completed, todo.id)));
    return Promise.all(toBeUpdated);
  },
};
