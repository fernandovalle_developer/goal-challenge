import { Module } from 'vuex';
import { Todo, TodoState, Visibility } from './types';
import { State } from '../types';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import modules from './modules';

export const TodoStore: Module<TodoState, State> = {
  state: {
    newTodo: new Todo(),
    todos: Array<Todo>(),
    editedTodo: new Todo(),
    visibility: Visibility.all,
  },
  getters,
  mutations,
  actions,
  modules,
};
