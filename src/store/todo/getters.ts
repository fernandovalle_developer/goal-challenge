import { GetterTree } from 'vuex';
import { State } from '../types';
import { TodoState, Visibility, Todo } from './types';

const getters: GetterTree<TodoState, State> = {
  newTodo(state: TodoState) {
    return state.newTodo;
  },
  todos(state: TodoState) {
    return state.todos;
  },
  editedTodo(state: TodoState) {
    return state.editedTodo;
  },
  visibility(state: TodoState) {
    return state.visibility;
  },
  filteredTodos(state: TodoState) {
    if (state.visibility === Visibility.active) {
      return state.todos.filter((todo:Todo) => !todo.completed);
    }
    if (state.visibility === Visibility.completed) {
      return state.todos.filter((todo:Todo) => todo.completed);
    }
    return state.todos;
  },
  remaining(state: TodoState) {
    return state.todos.filter((todo:Todo) => !todo.completed).length;
  },
  allDone(state: TodoState) {
    const nCompleted = state.todos.filter((todo:Todo) => todo.completed).length;
    return nCompleted === state.todos.length;
  },
};

export default getters;
