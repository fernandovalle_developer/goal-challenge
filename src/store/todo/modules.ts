import { ModuleTree } from 'vuex';
import { State } from '../types';

const modules: ModuleTree<State> = { };

export default modules;
