import { ActionTree } from 'vuex';
import apiTodos from '@/api/todos';
import { State } from '../types';
import { Todo, TodoState, Visibility } from './types';

const actions: ActionTree<TodoState, State> = {
  setTodoText({ commit }, todoText: string) {
    if (todoText) commit('SET_TODO', todoText);
    else commit('SET_TODO', new Todo());
  },
  completedUncompletedTodo({ commit }, todo: Todo) {
    const auxTodo = new Todo(todo.text, !todo.completed, todo.id);
    return apiTodos.update(auxTodo).then((response: Todo) => {
      commit('UPDATE_TODO', response);
    }).catch((err : Error) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err));
    });
  },
  addTodo({ commit }, todo: Todo) {
    if (todo.text.length > 0) {
      return apiTodos.insert(todo).then((response: Todo) => {
        commit('ADD_TODO', response);
        commit('CLEAR_TODO');
      }).catch((err : Error) => {
        // eslint-disable-next-line no-console
        console.log(JSON.stringify(err));
      });
    }
    return null;
  },
  removeTodo({ commit }, todo: Todo) {
    return apiTodos.delete(todo).then(() => {
      commit('REMOVE_TODO', todo);
    }).catch((err : Error) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err));
    });
  },
  setEditTodo({ commit }, todo: Todo) {
    commit('SET_EDIT_TODO', todo);
  },
  doneEditTodo({ commit }, todo: Todo) {
    return apiTodos.update(todo).then((response: Todo) => {
      commit('UPDATE_TODO', response);
      commit('SET_EDIT_TODO', new Todo());
    }).catch((err : Error) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err));
    });
  },
  cancelEditTodo({ commit }, todo: Todo) {
    return apiTodos.get(todo).then((response: Todo) => {
      commit('SET_EDIT_TODO', new Todo());
      commit('UPDATE_TODO', response);
    }).catch((err : Error) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err));
    });
  },
  completedUncompletedTodos(context) {
    return apiTodos.completedUncompletedTodos(context.getters.todos, !context.getters.allDone)
      .then(async (response) => {
        context.commit('SET_TODOS', response);
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  },
  setVisibility({ commit }, visibility: Visibility) {
    return commit('SET_VISIBILITY', visibility);
  },

  removeCompleted(context) {
    return apiTodos.clearCompleted(context.state.todos).then(() => (
      apiTodos.getTodos().then((resTodos) => {
        context.commit('SET_TODOS', resTodos);
      })
        .catch((error: Error) => {
          // eslint-disable-next-line no-console
          console.log(error);
        })))
      .catch((error: Error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  },
  fetchTodos({ commit }) {
    return apiTodos.getTodos().then((todos: Todo[]) => {
      commit('SET_TODOS', todos);
    }).catch((err : Error) => {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(err));
    });
  },
};

export default actions;
