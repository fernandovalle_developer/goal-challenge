export class Todo {
    id: undefined | number;

    text: string;

    completed: boolean;

    constructor(text = '', completed = false, id:undefined | number = undefined) {
      this.text = text;
      this.completed = completed;
      this.id = id;
    }
}

export enum Visibility {
  all = 'all',
  active = 'active',
  completed = 'completed',
}

export interface TodoState {
  newTodo: Todo,
  todos: Array<Todo>,
  editedTodo: Todo,
  visibility: Visibility,
}
