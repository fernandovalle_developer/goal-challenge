import { MutationTree } from 'vuex';
import { Todo, TodoState, Visibility } from './types';

const mutations: MutationTree<TodoState> = {
  SET_TODO(state: TodoState, text: string) {
    state.newTodo.text = text;
  },
  CLEAR_TODO(state: TodoState) {
    state.newTodo = new Todo();
  },
  ADD_TODO(state: TodoState, todo: Todo) {
    state.todos.push(new Todo(todo.text, todo.completed, todo.id));
  },
  UPDATE_TODO(state: TodoState, todo: Todo) {
    state.todos.splice(state.todos.findIndex((element) => element.id === todo.id), 1, todo);
  },
  REMOVE_TODO(state: TodoState, todo: Todo) {
    state.todos.splice(state.todos.findIndex((element) => element.id === todo.id), 1);
  },
  SET_TODOS(state: TodoState, todos: Todo[]) {
    state.todos.splice(0, state.todos.length);
    state.todos.push(...todos);
  },
  SET_EDIT_TODO(state: TodoState, todo: Todo) {
    state.editedTodo = todo;
  },
  COMPLETED_UNCOMPLETED_TODOS(state: TodoState, status: boolean) {
    state.todos.forEach((todo:Todo) => {
      todo.completed = status;
    });
  },
  SET_VISIBILITY(state: TodoState, visibility: Visibility) {
    state.visibility = visibility;
  },
  REMOVE_COMPLETED(state: TodoState) {
    state.todos = state.todos.filter((todo) => !todo.completed);
  },
};

export default mutations;
