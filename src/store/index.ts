import Vue from 'vue';
import Vuex from 'vuex';
import { State } from './types';
import { TodoStore } from './todo/index';

Vue.use(Vuex);

export default new Vuex.Store<State>({
  state: {
    loading: false,
  },
  getters: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    TodoStore,
  },
});
