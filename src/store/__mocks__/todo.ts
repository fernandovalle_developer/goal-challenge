import Vue from 'vue';
import Vuex from 'vuex';
import { Todo, Visibility } from '../todo/types';
import '../todo';

Vue.use(Vuex);

export const getters = {
  newTodo: jest.fn().mockReturnValue(new Todo()),
  todos: jest.fn().mockReturnValue([new Todo('My first Todo', false, 1), new Todo('My second Todo', true, 3), new Todo('My third Todo', false, 2)]),
  editedTodo: jest.fn().mockReturnValue(new Todo()),
  visibility: jest.fn().mockReturnValue(Visibility.all),
  filteredTodos: jest.fn().mockReturnValue([new Todo('My first Todo', false, 1), new Todo('My second Todo', true, 3), new Todo('My third Todo', false, 2)]),
  remaining: jest.fn().mockReturnValue(1),
  allDone: jest.fn().mockReturnValue(false),
};

export const mutations = {
  SET_TODO: jest.fn(),
  CLEAR_TODO: jest.fn(),
  ADD_TODO: jest.fn(),
  UPDATE_TODO: jest.fn(),
  REMOVE_TODO: jest.fn(),
  SET_TODOS: jest.fn(),
  SET_EDIT_TODO: jest.fn(),
  COMPLETED_UNCOMPLETED_TODOS: jest.fn(),
  SET_VISIBILITY: jest.fn(),
  REMOVE_COMPLETED: jest.fn(),
};

export const actions = {
  setTodoText: jest.fn(({ commit }) => { commit('SET_TODO'); }),
  completedUncompletedTodo: jest.fn(({ commit }) => { commit('UPDATE_TODO'); }),
  addTodo: jest.fn(({ commit }, todo: Todo) => { if (todo.text.length > 0) { commit('ADD_TODO'); commit('CLEAR_TODO'); } }),
  removeTodo: jest.fn(({ commit }) => { commit('REMOVE_TODO'); }),
  setEditTodo: jest.fn(({ commit }) => { commit('SET_EDIT_TODO'); }),
  doneEditTodo: jest.fn(({ commit }) => { commit('UPDATE_TODO'); commit('SET_EDIT_TODO'); }),
  cancelEditTodo: jest.fn(({ commit }) => { commit('UPDATE_TODO'); commit('SET_EDIT_TODO'); }),
  completedUncompletedTodos: jest.fn(({ commit }) => { commit('SET_TODOS'); }),
  setVisibility: jest.fn(({ commit }) => { commit('SET_VISIBILITY'); }),
  removeCompleted: jest.fn(({ commit }) => { commit('SET_TODOS'); }),
  fetchTodos: jest.fn(({ commit }) => { commit('SET_TODOS'); }),
};

export const state = {
  newTodo: new Todo(),
  todos: Array<Todo>(),
  editedTodo: new Todo(),
  visibility: Visibility.all,
};

export function createMocks(custom = {
  getters: {}, mutations: {}, actions: {}, state: {},
}) {
  const mockGetters = { ...getters, ...custom.getters };
  const mockMutations = { ...mutations, ...custom.mutations };
  const mockActions = { ...actions, ...custom.actions };
  const mockState = { ...state, ...custom.state };

  return {
    getters: mockGetters,
    mutations: mockMutations,
    actions: mockActions,
    state: mockState,
    store: new Vuex.Store({
      getters: mockGetters,
      mutations: mockMutations,
      actions: mockActions,
      state: mockState,
    }),
  };
}
