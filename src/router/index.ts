import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

function lazyLoad(view: string) {
  return () => import(`@/views/${view}.vue`);
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: lazyLoad('HomeView'),
  },
  {
    path: '/:visibility',
    name: 'home-filtered',
    component: lazyLoad('HomeView'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
