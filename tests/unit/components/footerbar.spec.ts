import { createLocalVue, shallowMount } from '@vue/test-utils';
import FooterBar from '@/components/todo/FooterBar.vue';
import Vuex from 'vuex';
import { createMocks } from '@/store/__mocks__/todo';
import { Visibility } from '@/store/todo/types';

jest.mock('@/store');
const localVue = createLocalVue();
localVue.use(Vuex);

describe('FooterBar.vue', () => {
  let wrapper:any;
  let storeMocks = createMocks();

  function prepareTest(customMocks?: { getters: {}; mutations: {}; actions: {}; state: {}; }) {
    storeMocks = createMocks(customMocks);
    wrapper = shallowMount(FooterBar, {
      store: storeMocks.store,
      localVue,
    });
  }

  it('footerbar should hide when no todos elements', () => {
    prepareTest();
    expect(wrapper.find('.footer').isVisible()).toBeTruthy();
    prepareTest({
      getters: {
        todos: jest.fn().mockReturnValue([]),
      },
      mutations: {},
      actions: {},
      state: {},
    });
    expect(wrapper.find('.footer').isVisible()).toBeFalsy();
  });

  it('showing remaining elements correctly', () => {
    prepareTest();
    expect(wrapper.find('.todo-count').text()).toEqual('1 item left');
    prepareTest({
      getters: {
        remaining: jest.fn().mockReturnValue(2),
      },
      mutations: {},
      actions: {},
      state: {},
    });
    expect(wrapper.find('.todo-count').text()).toEqual('2 items left');
  });

  it('current filter should be active and the others deactived', () => {
    prepareTest();
    expect(wrapper.find('.filters li:first-child a').classes()).toContainEqual('selected');
    expect(wrapper.find('.filters li:nth-child(2) a').classes()).not.toContainEqual('selected');
    expect(wrapper.find('.filters li:nth-child(3) a').classes()).not.toContainEqual('selected');
    prepareTest({
      getters: {
        visibility: jest.fn().mockReturnValue(Visibility.active),
      },
      mutations: {},
      actions: {},
      state: {},
    });
    expect(wrapper.find('.filters li:first-child a').classes()).not.toContainEqual('selected');
    expect(wrapper.find('.filters li:nth-child(2) a').classes()).toContainEqual('selected');
    expect(wrapper.find('.filters li:nth-child(3) a').classes()).not.toContainEqual('selected');
  });

  it('clear button fired correctly', () => {
    wrapper.find('.clear-completed').trigger('click');
    expect(storeMocks.mutations.SET_TODOS).toBeCalled();
  });
});
