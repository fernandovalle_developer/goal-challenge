import { mount, createLocalVue, shallowMount } from '@vue/test-utils';
import NewTodo from '@/components/todo/NewTodo.vue';
import Vuex from 'vuex';
import { createMocks } from '@/store/__mocks__/todo';
import { Todo } from '@/store/todo/types';

jest.mock('@/store');
const localVue = createLocalVue();
localVue.use(Vuex);

describe('NewTodo.vue', () => {
  let wrapper:any;
  let storeMocks = createMocks();

  function prepareTest(customMocks?: { getters: {}; mutations: {}; actions: {}; state: {}; }) {
    storeMocks = createMocks(customMocks);
    wrapper = mount(NewTodo, {
      attachTo: document.body, // needed for lookup for the activeElement
      store: storeMocks.store,
      localVue,
    });
  }

  it('is Newtodo input focused at the begining', () => {
    prepareTest();
    expect(document.activeElement?.id).toEqual('new-todo-input');
  });

  it('No empty element is added in todos list', () => {
    prepareTest();
    wrapper.find('input').trigger('keyup.enter');
    expect(storeMocks.mutations.ADD_TODO).not.toBeCalled();
  });

  it('is NewTodo set in vuex', () => {
    prepareTest();
    wrapper.find('input').setValue('My first todo');
    expect(storeMocks.mutations.SET_TODO).toBeCalled();
  });

  it('is NewTodo added in todos list', () => {
    prepareTest({
      getters: {
        newTodo: jest.fn().mockReturnValue(new Todo('My first todo', false, undefined)),
      },
      mutations: {},
      actions: {},
      state: {},
    });
    wrapper.find('input').trigger('keyup.enter');
    expect(storeMocks.mutations.ADD_TODO).toBeCalled();
  });
});
