import { createLocalVue, shallowMount } from '@vue/test-utils';
import TodoList from '@/components/todo/TodoList.vue';
import Vuex from 'vuex';
import { createMocks } from '@/store/__mocks__/todo';
import { Todo, Visibility } from '@/store/todo/types';

jest.mock('@/store');
const localVue = createLocalVue();
localVue.use(Vuex);

describe('TodoList.vue', () => {
  let wrapper:any;
  let storeMocks = createMocks();

  function prepareTest(customMocks?: { getters: {}; mutations: {}; actions: {}; state: {}; }) {
    storeMocks = createMocks(customMocks);
    wrapper = shallowMount(TodoList, {
      store: storeMocks.store,
      localVue,
    });
  }

  it('Todo list should hide when no todos elements', () => {
    prepareTest();
    expect(wrapper.find('.main').isVisible()).toBeTruthy();
    prepareTest({
      getters: {
        todos: jest.fn().mockReturnValue([]),
      },
      mutations: {},
      actions: {},
      state: {},
    });
    expect(wrapper.find('.main').isVisible()).toBeFalsy();
  });

  it('toggle-all checkbox is changing his state properly', () => {
    prepareTest();
    expect(wrapper.find('#toggle-all').element.checked).toBeFalsy();
    prepareTest({
      getters: {
        allDone: jest.fn().mockReturnValue(true),
      },
      mutations: {},
      actions: {},
      state: {},
    });
    expect(wrapper.find('#toggle-all').element.checked).toBeTruthy();
  });

  it('renders properly the todos list', () => {
    prepareTest();
    expect(wrapper.findAll('li').length).toBe(3); // Visibility.active
    prepareTest({
      getters: {
        filteredTodos: jest.fn().mockReturnValue([new Todo('My first Todo', false, 1), new Todo('My third Todo', false, 2)]),
        visibility: jest.fn().mockReturnValue(Visibility.active),
      },
      mutations: {},
      actions: {},
      state: {},
    });
    expect(wrapper.findAll('li').length).toBe(2); // Visibility.active
  });

  it('renders properly every element in list', () => {
    prepareTest();
    expect(wrapper.find('#todo-1 + label').text().trim()).toEqual('My first Todo');
    expect(wrapper.find('li:first-child').classes().toString()).not.toContain('completed');
    expect(wrapper.find('li:nth-child(2)').classes().toString()).toContain('completed');
  });

  it('testing destroy button', () => {
    prepareTest();
    wrapper.find('#todo-1 + label + button').trigger('click');
    expect(storeMocks.mutations.REMOVE_TODO).toBeCalled();
  });

  it('double clicking todo element list should edit element', () => {
    prepareTest();
    wrapper.find('#todo-1 + label').trigger('dblclick');
    expect(storeMocks.mutations.SET_EDIT_TODO).toBeCalled();
  });

  it('double clicking todo element list should edit element', () => {
    prepareTest();
    wrapper.find('#todo-1 + label').trigger('dblclick');
    expect(storeMocks.mutations.SET_EDIT_TODO).toBeCalled();
  });

  it('doing interactions with todo input', () => {
    prepareTest();
    wrapper.find('.todo .edit').trigger('blur');
    expect(storeMocks.mutations.UPDATE_TODO).toBeCalled();
    expect(storeMocks.mutations.SET_EDIT_TODO).toBeCalled();
    wrapper.find('.todo .edit').trigger('keyup.enter');
    expect(storeMocks.mutations.UPDATE_TODO).toBeCalled();
    expect(storeMocks.mutations.SET_EDIT_TODO).toBeCalled();
    wrapper.find('.todo .edit').trigger('keyup.esc');
    expect(storeMocks.mutations.UPDATE_TODO).toBeCalled();
    expect(storeMocks.mutations.SET_EDIT_TODO).toBeCalled();
  });
});
