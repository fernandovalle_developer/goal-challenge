import Vuex from 'vuex';
import { Todo, TodoState, Visibility } from '@/store/todo/types';
import { createLocalVue } from '@vue/test-utils';
import getters from '@/store/todo/getters';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('getters', () => {
  let store = new Vuex.Store({});
  const defaultTodos = [
    new Todo('First todo', true, 1),
    new Todo('Second todo', false, 2),
    new Todo('Third todo', true, 3),
  ];

  function setState(visibility:Visibility = Visibility.all, todos:Todo[] = defaultTodos) {
    const state:TodoState = {
      visibility,
      todos,
      newTodo: new Todo(),
      editedTodo: new Todo('Second todo', false, 2),
    };

    store = new Vuex.Store({
      state: {
        loading: false,
      },
      modules: {
        TodoStore: {
          state,
          getters,
        },
      },
    });
  }

  setState(Visibility.active);

  it('newTodo', () => {
    expect(store.getters.newTodo.text).toEqual('');
    expect(store.getters.newTodo.completed).toBeFalsy();
    expect(store.getters.newTodo.id).toBeUndefined();
  });

  it('todos', () => {
    expect(store.getters.todos).toEqual([
      new Todo('First todo', true, 1),
      new Todo('Second todo', false, 2),
      new Todo('Third todo', true, 3),
    ]);
  });

  it('editedTodo', () => {
    expect(store.getters.editedTodo.text).toEqual('Second todo');
  });

  it('visibility', () => {
    expect(store.getters.visibility).toEqual(Visibility.active);
  });

  it('filteredTodos', () => {
    setState(Visibility.all);
    expect(store.getters.filteredTodos).toEqual([
      new Todo('First todo', true, 1),
      new Todo('Second todo', false, 2),
      new Todo('Third todo', true, 3),
    ]);
    setState(Visibility.active);
    expect(store.getters.filteredTodos).toEqual([
      new Todo('Second todo', false, 2),
    ]);
    setState(Visibility.completed);
    expect(store.getters.filteredTodos).toEqual([
      new Todo('First todo', true, 1),
      new Todo('Third todo', true, 3),
    ]);
  });

  it('remaining', () => {
    expect(store.getters.remaining).toBe(1);
  });

  it('allDone', () => {
    expect(store.getters.allDone).toBeFalsy();
    setState(Visibility.all, [
      new Todo('First todo', true, 1),
      new Todo('Second todo', true, 2)]);
    expect(store.getters.allDone).toBeTruthy();
  });
});
