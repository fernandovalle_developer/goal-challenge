import Vuex from 'vuex';
import { Todo, TodoState, Visibility } from '@/store/todo/types';
import { createLocalVue } from '@vue/test-utils';
import { TodoStore } from '@/store/todo';
// import mockPosts from "./__mocks__/posts.json";

jest.mock('@/api/todos', () => {
  const api = jest.requireActual('@/api/todos');
  api.BASE_URL = 'http://localhost:3001/todos';
  return api;
});

const localVue = createLocalVue();
localVue.use(Vuex);

describe('actions', () => {
  const store = new Vuex.Store({
    modules: {
      TodoStore,
    },
  });

  it('fetchTodos', async () => {
    await store.dispatch('fetchTodos');
    expect(store.getters.todos).toEqual([
      { id: 1, text: 'My todo 1', completed: false },
      { id: 2, text: 'My todo 2', completed: true },
      { id: 3, text: 'My todo 3', completed: false },
    ]);
  });

  it('setTodoText', async () => {
    await store.dispatch('setTodoText', 'Text for newtodo');
    expect(store.getters.newTodo).toEqual(new Todo('Text for newtodo', false, undefined));
  });
  it('completedUncompletedTodo', async () => {
    await store.dispatch('completedUncompletedTodo', new Todo('My todo 3', false, 3));
    expect(store.getters.todos.find((todo:Todo) => todo.id == 3)).toEqual(new Todo('My todo 3', true, 3));
  });

  it('addTodo', async () => {
    await store.dispatch('addTodo', new Todo('My todo 4', false, undefined));
    expect(store.getters.todos.length).toEqual(4);
    expect(store.getters.todos[3]).toEqual(new Todo('My todo 4', false, 4));
  });

  it('removeTodo', async () => {
    await store.dispatch('removeTodo', new Todo('My todo 3', false, 3));
    expect(store.getters.todos.length).toEqual(3);
    expect(store.getters.todos.find((todo:Todo) => todo.id == 3)).toBeUndefined;
  });

  it('setEditTodo && doneEditTodo', async () => {
    const todo = new Todo('My todo 4 changed', false, 4);
    await store.dispatch('setEditTodo', todo);
    expect(store.getters.editedTodo).toEqual(todo);
    await store.dispatch('doneEditTodo', todo);
    expect(store.getters.editedTodo).toEqual(new Todo());
    expect(store.getters.todos.find((todo:Todo) => todo.id == 4)).toEqual(todo);
  });

  it('cancelEditTodo', async () => {
    const todo = new Todo('edited Todo', false, 1);
    await store.dispatch('cancelEditTodo');
    expect(store.getters.editedTodo).toEqual(new Todo());
    expect(store.getters.todos[store.getters.todos.length - 1]).not.toEqual(todo);
  });

  it('setVisibility', async () => {
    await store.dispatch('setVisibility', Visibility.active);
    expect(store.getters.visibility).toEqual(Visibility.active);
    expect(store.getters.filteredTodos).toEqual(store.getters.todos.filter((todo:Todo) => !todo.completed));

    await store.dispatch('setVisibility', Visibility.all);
    expect(store.getters.visibility).toEqual(Visibility.all);
    expect(store.getters.filteredTodos).toEqual(store.getters.todos);

    await store.dispatch('setVisibility', Visibility.completed);
    expect(store.getters.visibility).toEqual(Visibility.completed);
    expect(store.getters.filteredTodos).toEqual(store.getters.todos.filter((todo:Todo) => todo.completed));
  });

  it('completedUncompletedTodos', async () => {
    await store.dispatch('completedUncompletedTodos'); // Set all completed
    expect(store.getters.todos.filter((todo:Todo) => todo.completed).length).toEqual(store.getters.todos.length);
    await store.dispatch('completedUncompletedTodos'); // Set all uncompleted
    expect(store.getters.todos.filter((todo:Todo) => !todo.completed).length).toEqual(store.getters.todos.length);
  });

  it('removeCompleted', async () => {
    await store.dispatch('completedUncompletedTodos'); // Set all complete
    console.log(JSON.stringify(store.getters.todos));
    await store.dispatch('removeCompleted');
    expect(store.getters.todos.filter((element:Todo) => element.completed).length).toEqual(0);
  });
});
