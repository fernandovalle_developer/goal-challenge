import mutations from '@/store/todo/mutations';
import { Todo, TodoState, Visibility } from '@/store/todo/types';

describe('mutations', () => {
  const state:TodoState = {
    newTodo: new Todo(),
    todos: [],
    editedTodo: new Todo(),
    visibility: Visibility.all,
  };

  // Tests for current editing Todo
  it('SET_TODO', () => {
    mutations.SET_TODO(state, 'My first Todo');
    expect(state.newTodo.text).toEqual('My first Todo');
    expect(state.newTodo.completed).toBeFalsy;
  });

  it('CLEAR_TODO', () => {
    mutations.CLEAR_TODO(state);
    expect(state.newTodo.text).toEqual('');
  });

  it('ADD_TODO', () => {
    const todo = new Todo('My first todo', false, 1);
    mutations.ADD_TODO(state, todo);
    expect(state.todos.length).toEqual(1);
    expect(state.todos[0]).toEqual(todo);
  });

  it('UPDATE_TODO', () => {
    const todo = new Todo('My first todo changed', false, 1);
    mutations.UPDATE_TODO(state, todo);
    expect(state.todos[0]).toEqual(todo);
  });

  it('REMOVE_TODO', () => {
    const todo = new Todo('My first todo changed', false, 1);
    mutations.REMOVE_TODO(state, todo);
    expect(state.todos.length).toEqual(0);
  });

  it('SET_TODOS', () => {
    const todos = [new Todo('My first todo', false, 1), new Todo('My second todo', false, 1)];
    mutations.SET_TODOS(state, todos);
    expect(state.todos.length).toEqual(2);
  });

  it('SET_EDIT_TODO', () => {
    mutations.SET_EDIT_TODO(state, new Todo('My first Todo', false, 1));
    expect(state.editedTodo.text).toEqual('My first Todo');
  });

  it('COMPLETED_UNCOMPLETED_TODOS', () => {
    const todos = [new Todo('My first todo', true, 1), new Todo('My second todo', false, 1)];
    mutations.SET_TODOS(state, todos);
    mutations.COMPLETED_UNCOMPLETED_TODOS(state, true);
    expect(state.todos.filter((todo:Todo) => todo.completed).length).toEqual(2);
    mutations.COMPLETED_UNCOMPLETED_TODOS(state, false);
    expect(state.todos.filter((todo:Todo) => !todo.completed).length).toEqual(2);
  });

  it('SET_VISIBILITY', () => {
    mutations.SET_VISIBILITY(state, Visibility.active);
    expect(state.visibility).toEqual(Visibility.active);
    mutations.SET_VISIBILITY(state, Visibility.all);
    expect(state.visibility).toEqual(Visibility.all);
    mutations.SET_VISIBILITY(state, Visibility.completed);
    expect(state.visibility).toEqual(Visibility.completed);
  });

  it('REMOVE_COMPLETED', () => {
    const todos = [new Todo('My first todo', true, 1), new Todo('My second todo', false, 0)];
    mutations.SET_TODOS(state, todos);
    mutations.REMOVE_COMPLETED(state);
    expect(state.todos.length).toEqual(1);
  });
});
