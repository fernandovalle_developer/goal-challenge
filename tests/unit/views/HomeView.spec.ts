import { createLocalVue, mount } from '@vue/test-utils';
import VueRouter from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import Vuex from 'vuex';
import { createMocks } from '@/store/__mocks__/todo';
import { Visibility } from '@/store/todo/types';
import router from '@/router';

jest.mock('@/store');
const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueRouter);

describe('HomeView.vue', () => {
  let wrapper:any;
  let storeMocks = createMocks();

  function prepareTest(customMocks?: { getters: {}; mutations: {}; actions: {}; state: {}; }) {
    storeMocks = createMocks(customMocks);
    wrapper = mount(HomeView, {
      store: storeMocks.store,
      localVue,
      router,
    });
  }

  it('Every component is loaded correctly', () => {
    prepareTest();
    expect(wrapper.find('.new-todo')).not.toBeUndefined();
    expect(wrapper.find('.main')).not.toBeUndefined();
    expect(wrapper.find('.footer')).not.toBeUndefined();
  });

  it('watcher should set visibility when route changed', () => {
    prepareTest();
    router.push('/active');
    expect(storeMocks.mutations.SET_VISIBILITY).toBeCalled();
  });
});
