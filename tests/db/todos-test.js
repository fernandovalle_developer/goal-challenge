// index.js
module.exports = () => {
  const data = { todos: [] };
  data.todos.push({ id: 1, text: 'My todo 1', completed: false });
  data.todos.push({ id: 2, text: 'My todo 2', completed: true });
  data.todos.push({ id: 3, text: 'My todo 3', completed: false });
  return data;
};
