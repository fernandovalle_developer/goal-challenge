# goal-challenge - Vue Todo list
This poroject includes Vuejs framework, json-server as mocked backend and testing with Jest.

## Project setup
```
yarn install
```

### Development
First of all run json-server-test:
```
yarn json-server
```
Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Testing
First of all run json-server-test:
```
yarn json-server-test
```
You can run now test units:
```
yarn test:unit
```